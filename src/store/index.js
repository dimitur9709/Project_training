import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    team: [
      { firstName: "Ivan",lastName:"Ivanov", age: "22" },
      { firstName: "Peter",lastName:"Petrov", age: "22"}
    ]
  },
  getters: {
    count: state => {
      return state.team.length;
    }
  },
  mutations: {
    ADD_TO_TEAM: (state, member) => {
      state.team.push(member);

    },
    REMOVE_FROM_TEAM: (state, index) => {
      state.team.splice(index, 1);
    },
  },
  actions: {},
  modules: {}
});
